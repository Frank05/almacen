

const { showAllTshirt } = require('../../libs');
const { verifyToken } = require('../../middleware')

const router = require('express').Router()

router.get('/showAllTshirt', verifyToken, async (req, res) => {
  try {
    const allTshirt = await showAllTshirt()

    // Response the request
    res.status(200).json(allTshirt)

  } catch (error) {
    console.log('Error:', error.message);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}