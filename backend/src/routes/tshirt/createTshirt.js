//const { Tshirt } = require('../../models')
//const {  } = require('../../libs')
const { createTshirt } = require('../../libs')
const { verifyToken } = require('../../middleware')

const router = require('express').Router()

router.post('/createTshirt', verifyToken, async (req, res) => {
  try {
    const { marca_tshirt, talla_tshirt, precio_tshirt, cantidad_tshirt, colour_tshirt } = req.body

    await createTshirt(
      marca_tshirt,
      talla_tshirt,
      precio_tshirt,
      cantidad_tshirt,
      colour_tshirt
    )

    // Response the request
    res.status(200).json({
      message: 'Tshirt has been saved successfully',
      marca_tshirt,
      talla_tshirt,
      precio_tshirt,
      cantidad_tshirt,
      colour_tshirt
    })

  } catch (error) {
    console.log('Error:', error.message);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}