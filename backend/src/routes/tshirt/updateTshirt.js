//const { Tshirt } = require('../models')

const { updateTshirt } = require('../../libs')

const router = require('express').Router()

router.post('/updateTshirt', async (req, res) => {
  try {
    const { id, marca_tshirt, talla_tshirt, precio_tshirt, cantidad_tshirt, colour_tshirt } = req.body

    await updateTshirt(id,
      marca_tshirt,
      talla_tshirt,
      precio_tshirt,
      cantidad_tshirt,
      colour_tshirt
    )

    // Response the request
    res.status(200).json({
      message: 'T-shirt has been updated successfully',
      marca_tshirt,
      talla_tshirt,
      precio_tshirt,
      cantidad_tshirt,
      colour_tshirt
    })

  } catch (error) {
    console.log('Error:', error.message);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}