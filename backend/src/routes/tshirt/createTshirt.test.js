const mongoose = require('mongoose')

const supertest = require('supertest')
const { app } = require('../../app')

const api = supertest(app)

// jest.setTimeout(10 * 1000)

describe('Integration testing to /createTshirt', () => {

  beforeAll(async () => {
    // Waiting for database
    let connected = false
    while (!connected) {
      try {
        const uri = `mongodb://${process.env.MONGO_INITDB_HOST}`
        const options = {
          user: process.env.MONGO_INITDB_ROOT_USERNAME,
          pass: process.env.MONGO_INITDB_ROOT_PASSWORD,
          dbName: process.env.MONGO_INITDB_DATABASE,
        }
        await mongoose.connect(uri, options)
        console.log('Connected with database!');
        connected = true
      } catch (error) {
        console.log('Trying to connect with database...');
        console.log(error);
        await new Promise(resolve => setTimeout(resolve, 3000))
      }
    }
  })

  afterAll(async () => {
    await mongoose.connection.close()
  })


  test('should ok - status 200', async () => {
    // Prepare
    expect.assertions(6)

    const marca_tshirt = 'Totto'
    const talla_tshirt = 7
    const precio_tshirt = 20
    const cantidad_tshirt = 3
    const colour_tshirt = "black"

    // Test
    const response = await api
      .post('/createTshirt')
      .send({
        marca_tshirt,
        talla_tshirt,
        precio_tshirt,
        cantidad_tshirt,
        colour_tshirt
      })
      .expect(200)

    expect(response.body.message).toBe('Tshirt has been saved successfully')
    expect(response.body.marca_tshirt).toBe(marca_tshirt)
    expect(response.body.talla_tshirt).toBe(talla_tshirt)
    expect(response.body.precio_tshirt).toBe(precio_tshirt)
    expect(response.body.cantidad_tshirt).toBe(cantidad_tshirt)
    expect(response.body.colour_tshirt).toBe(colour_tshirt)
  })

  test('should error - status 500', async () => {
    // Prepare
    expect.assertions(1)

    const marca_tshirt = undefined
    const talla_tshirt = 9
    const precio_tshirt = 21.5
    const cantidad_tshirt = 3
    const colour_tshirt = "blue"

    // Test
    const response = await api
      .post('/createTshirt')
      .send({
        marca_tshirt,
        talla_tshirt,
        precio_tshirt,
        cantidad_tshirt,
        colour_tshirt
      })
      .expect(500)

    expect(response.body.message).toBe('marca_tshirt invalid - received: undefined')
  })

})