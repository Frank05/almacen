//const { Tshirt } = require('../models')
const { deleteTshirt } = require("../../libs")

const router = require('express').Router()

router.post('/deleteTshirt', async (req, res) => {
  try {
    const { id } = req.body
    //console.log('id', id)

    await deleteTshirt(id)

    // Response the request
    res.status(200).json({
      message: 'Tshirt has been deleted successfully',
      id,
    })

  } catch (error) {
    console.log('Error:', error.message);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}