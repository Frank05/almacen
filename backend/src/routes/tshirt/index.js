const { router: createTshirt } = require('./createTshirt')
const { router: deleteTshirt } = require("./deleteTshirt")
const { router: showAllTshirt } = require("./showAllTshirt")
const { router: updateTshirt } = require("./updateTshirt")

module.exports = {
  router: [
    createTshirt,
    deleteTshirt,
    showAllTshirt,
    updateTshirt
  ]
}