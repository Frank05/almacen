const { router: createShoe } = require('./createShoe')
const { router: deleteShoe } = require('./deleteShoe')
const { router: showAllShoes } = require('./showAllShoes')
const { router: updateShoe } = require('./updateShoe')

module.exports = {
  router: [
    createShoe,
    deleteShoe,
    showAllShoes,
    updateShoe
  ]
}