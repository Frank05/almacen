const { createShoe } = require('../../libs')

const router = require('express').Router()

router.post('/createShoe', async (req, res) => {
  try {
    // Get data from frontend
    const { marca_shoe, talla_shoe, precio_shoe, cantidad_shoe } = req.body

    // Save data
    await createShoe(
      marca_shoe,
      talla_shoe,
      precio_shoe,
      cantidad_shoe
    )

    // Response the request
    res.status(200).json({
      message: 'Shoe has been saved successfully',
      marca_shoe,
      talla_shoe,
      precio_shoe,
      cantidad_shoe,
    })

  } catch (error) {
    console.log('Error:', error.message);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}