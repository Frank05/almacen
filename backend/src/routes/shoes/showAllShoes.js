//const { Shoes } = require('../../models');
const { showAllShoes } = require('../../libs');


const router = require('express').Router()

router.get('/showAllShoes', async (req, res) => {
  try {
    //const allShoes = await Shoes.find()
    const allShoes = await showAllShoes()

    // Response the request
    res.status(200).json(allShoes)

  } catch (error) {
    console.log('Error:', error.message);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}