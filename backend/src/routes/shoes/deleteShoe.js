//const { Shoes } = require('../../models')
const { deleteShoe } = require("../../libs")

const router = require('express').Router()

router.post('/deleteShoe', async (req, res) => {
  try {
    //Esto es desestructuracion ojo
    // const id = req.body.id    // Opcion 1 
    const { id } = req.body   // Opcion 2 - Mas moderno
    //console.log('id', id)


    // Opcion 1
    // const id = req.body.id
    // const marca = req.body.marca
    // const talla = req.body.talla

    // // Opcion 2 - moderna
    // const { id, marca, talla } = req.body 

    await deleteShoe(id)
    //Shoes.findOneAndDelete(id)

    // Response the request
    res.status(200).json({
      message: 'Shoe has been deleted successfully',
      id,
    })

  } catch (error) {
    console.log('Error:', error.message);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}