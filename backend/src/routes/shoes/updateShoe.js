//const { Shoes } = require('../../models')
const { updateShoe } = require('../../libs')

const router = require('express').Router()

router.post('/updateShoe', async (req, res) => {
  try {
    const { id, marca_shoe, talla_shoe, precio_shoe, cantidad_shoe } = req.body

    await updateShoe(
      id,
      marca_shoe,
      talla_shoe,
      precio_shoe,
      cantidad_shoe
    )

    // Response the request
    res.status(200).json({
      message: 'Shoe has been updated successfully',
      marca_shoe,
      talla_shoe,
      precio_shoe,
      cantidad_shoe,
    })

  } catch (error) {
    console.log('Error:', error.message);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}