const mongoose = require('mongoose')

const supertest = require('supertest')
const { app } = require('../../app')

const api = supertest(app)

// jest.setTimeout(10 * 1000)

describe('Integration testing to /createShoe', () => {

  beforeAll(async () => {
    // Waiting for database
    let connected = false
    while (!connected) {
      try {
        const uri = `mongodb://${process.env.MONGO_INITDB_HOST}`
        const options = {
          user: process.env.MONGO_INITDB_ROOT_USERNAME,
          pass: process.env.MONGO_INITDB_ROOT_PASSWORD,
          dbName: process.env.MONGO_INITDB_DATABASE,
        }
        await mongoose.connect(uri, options)
        console.log('Connected with database!');
        connected = true
      } catch (error) {
        console.log('Trying to connect with database...');
        console.log(error);
        await new Promise(resolve => setTimeout(resolve, 3000))
      }
    }
  })

  afterAll(async () => {
    await mongoose.connection.close()
  })

  
  test('should ok - status 200', async () => {
    // Prepare
    expect.assertions(5)

    const marca_shoe = 'Venus'
    const talla_shoe = 9
    const precio_shoe = 21.5
    const cantidad_shoe = 3

    // Test
    const response = await api
      .post('/createShoe')
      .send({
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe
      })
      .expect(200)

    expect(response.body.message).toBe('Shoe has been saved successfully')
    expect(response.body.marca_shoe).toBe(marca_shoe)
    expect(response.body.talla_shoe).toBe(talla_shoe)
    expect(response.body.precio_shoe).toBe(precio_shoe)
    expect(response.body.cantidad_shoe).toBe(cantidad_shoe)
  })

  test('should error - status 500', async () => {
    // Prepare
    expect.assertions(1)

    const marca_shoe = undefined
    const talla_shoe = 9
    const precio_shoe = 21.5
    const cantidad_shoe = 3

    // Test
    const response = await api
      .post('/createShoe')
      .send({
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe
      })
      .expect(500)

    expect(response.body.message).toBe('marca_shoe invalid - received: undefined')
  })

})