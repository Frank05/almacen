const { router: routerLogin } = require('./login')

module.exports = {
  router: [
    routerLogin
  ]
}