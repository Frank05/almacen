const router = require('express').Router()
const { createToken } = require('../../libs')

router.post('/login', async (req, res) => {
  try {
    const { user, password } = req.body

    // Search in DB the user (_id, user, password)

    // user = admin
    // password = Fr4nkl1n


    if (user === 'admin' && password === 'Fr4nkl1n') {
      // Generar el token y envia al frontend
      const token = createToken(user)

      // Response
      res.status(200).json({
        message: `Welcome ${user}`,
        token
      })

    } else {
      throw {
        message: "Wrong credentials",
        route: '/login',
        method: 'post'
      }
    }

  } catch (error) {
    console.log('Error:', error);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}