const { router: createHoodie } = require('./createHoodie')
const { router: deleteHoodie } = require('./deleteHoodie')
const { router: showAllHoodie } = require('./showAllHoodie')
const { router: updateHoodie } = require('./updateHoodie')

module.exports = {
  router: [
    createHoodie,
    deleteHoodie,
    showAllHoodie,
    updateHoodie
  ]
}