
const mongoose = require('mongoose')
const supertest = require('supertest')
const { app } = require("../../app")

const api = supertest(app)

describe('Integration testing to /deleteHoodie', () => {

  beforeAll(async () => {
    // Waiting for database
    let connected = false
    while (!connected) {
      try {
        const uri = `mongodb://${process.env.MONGO_INITDB_HOST}`
        const options = {
          user: process.env.MONGO_INITDB_ROOT_USERNAME,
          pass: process.env.MONGO_INITDB_ROOT_PASSWORD,
          dbName: process.env.MONGO_INITDB_DATABASE,
        }
        await mongoose.connect(uri, options)
        console.log('Connected with database!');
        connected = true
      } catch (error) {
        console.log('Trying to connect with database...');
        console.log(error);
        await new Promise(resolve => setTimeout(resolve, 3000))
      }
    }
  })

  afterAll(async () => {
    await mongoose.connection.close()
  })

  test('should ok - status 200', async () => {
    // Prepare
    expect.assertions(1)

    const id = "636879d08159ec8a8355a274"


    // Test
    const response = await api
      .post('/deleteHoodie')
      .send({
        id
      })
      .expect(200)
    console.log('response', response)

    expect(response.body.message).toBe('Hoodie has been deleted successfully')

  })



})