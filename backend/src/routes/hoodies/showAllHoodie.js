const { showAllHoodie } = require('../../libs');
//const { Hoodie } = require('../../models');

const router = require('express').Router()

router.get('/showAllHoodie', async (req, res) => {
  try {
    const allHoodie = await showAllHoodie()

    // Response the request
    res.status(200).json(allHoodie)

  } catch (error) {
    console.log('Error:', error.message);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}