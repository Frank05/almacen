//const { Hoodie } = require('../../models')
const { deleteHoodie } = require("../../libs")
//const { Hoodie } = require("../../models")

const router = require('express').Router()

router.post('/deleteHoodie', async (req, res) => {
  try {
    const { id } = req.body
    //console.log('id', id)

    await deleteHoodie(id)

    // Response the request
    res.status(200).json({
      message: 'Hoodie has been deleted successfully',
      id,
    })

  } catch (error) {
    console.log('Error:', error.message);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}