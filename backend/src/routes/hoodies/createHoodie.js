const { createHoodie } = require('../../libs')

const router = require('express').Router()

router.post('/createHoodie', async(req, res) => {
  try {
    //Get data from frontend
    const { marca_hoodie, talla_hoodie, precio_hoodie, cantidad_hoodie, colour_hoodie } = req.body
    //console.log('frank'),
    await createHoodie(
      marca_hoodie,
      talla_hoodie,
      precio_hoodie,
      cantidad_hoodie,
      colour_hoodie
    )
    //console.log('createHoodie', createHoodie)
    // Response the request
    res.status(200).json({
      message: 'Hoodie has been saved successfully',
      marca_hoodie, 
      talla_hoodie, 
      precio_hoodie, 
      cantidad_hoodie, 
      colour_hoodie
    })

  } catch (error) {
    console.log('Error:', error.message);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}