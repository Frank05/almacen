const mongoose = require('mongoose')

const supertest = require('supertest')
const { app } = require('../../app')

const api = supertest(app)

// jest.setTimeout(10 * 1000)

describe('Integration testing to /createHoodie', () => {

  beforeAll(async () => {
    // Waiting for database
    let connected = false
    while (!connected) {
      try {
        const uri = `mongodb://${process.env.MONGO_INITDB_HOST}`
        const options = {
          user: process.env.MONGO_INITDB_ROOT_USERNAME,
          pass: process.env.MONGO_INITDB_ROOT_PASSWORD,
          dbName: process.env.MONGO_INITDB_DATABASE,
        }
        await mongoose.connect(uri, options)
        console.log('Connected with database!');
        connected = true
      } catch (error) {
        console.log('Trying to connect with database...');
        console.log(error);
        await new Promise(resolve => setTimeout(resolve, 3000))
      }
    }
  })

  afterAll(async () => {
    await mongoose.connection.close()
  })


  test('should ok - status 200', async () => {
    // Prepare
    expect.assertions(6)

    const marca_hoodie = 'Venus'
    const talla_hoodie = 9
    const precio_hoodie = 21.5
    const cantidad_hoodie = 3
    const colour_hoodie = "blue"

    // Test
    const response = await api
      .post('/createHoodie')
      .send({
        marca_hoodie,
        talla_hoodie,
        precio_hoodie,
        cantidad_hoodie,
        colour_hoodie
      })
      .expect(200)

    expect(response.body.message).toBe('Hoodie has been saved successfully')
    expect(response.body.marca_hoodie).toBe(marca_hoodie)
    expect(response.body.talla_hoodie).toBe(talla_hoodie)
    expect(response.body.precio_hoodie).toBe(precio_hoodie)
    expect(response.body.cantidad_hoodie).toBe(cantidad_hoodie)
    expect(response.body.colour_hoodie).toBe(colour_hoodie)
  })

  test('should error - status 500', async () => {
    // Prepare
    expect.assertions(1)

    const marca_hoodie = undefined
    const talla_hoodie = 9
    const precio_hoodie = 21.5
    const cantidad_hoodie = 3
    const colour_hoodie = "blue"

    // Test
    const response = await api
      .post('/createHoodie')
      .send({
        marca_hoodie,
        talla_hoodie,
        precio_hoodie,
        cantidad_hoodie,
        colour_hoodie
      })
      .expect(500)

    expect(response.body.message).toBe('marca_hoodie invalid - received: undefined')
  })

})