const { updateHoodie } = require('../../libs/hoodies/updateHoodie')
//const { Hoodie } = require('../../models')

const router = require('express').Router()

router.post('/updateHoodie', async (req, res) => {
  try {
    const { id, marca_hoodie, talla_hoodie, precio_hoodie, cantidad_hoodie, colour_hoodie } = req.body

    await updateHoodie(id,
      marca_hoodie,
      talla_hoodie,
      precio_hoodie,
      cantidad_hoodie,
      colour_hoodie
    )

    // Response the request
    res.status(200).json({
      message: 'Hoodie has been updated successfully',
      marca_hoodie,
      talla_hoodie,
      precio_hoodie,
      cantidad_hoodie,
      colour_hoodie
    })

  } catch (error) {
    console.log('Error:', error.message);
    res.status(500).send({ message: error.message })
  }
})

module.exports = {
  router
}