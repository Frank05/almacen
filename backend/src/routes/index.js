const { router: routerShoes } = require("./shoes/")
const { router: routerHoodies } = require("./hoodies/")
const { router: routerTshirt } = require("./tshirt/")
const { router: routerLogin } = require('./login')

module.exports = {
  router: [
    ...routerShoes,
    ...routerHoodies,
    ...routerTshirt,
    ...routerLogin
  ]
}