const { updateShoe } = require('./updateShoe')

// Mocks
const { Shoes } = require('../../models/Shoes')
jest.mock('../../models/Shoes')


describe('Testing to updateShoes.js', () => {

  test('should error - marca_shoe undefined', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_shoe = undefined
    const talla_shoe = 9
    const precio_shoe = 10.75
    const cantidad_shoe = 5
    const colour_shoe = "yellow"

    // Test
    try {
      const response = await updateShoe(
        id,
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe,
        colour_shoe
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('marca_shoe invalid - received: undefined')
    }
  })

  test('should error - id undefined', async () => {
    // Prepare
    expect.assertions(1)
    const id = undefined
    const marca_shoe = 'Test marca_shoe'
    const talla_shoe = 37
    const precio_shoe = 10.75
    const cantidad_shoe = 5
    const colour_shoe = "yellow"


    // Test
    try {
      const response = await updateShoe(
        id,
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe,
        colour_shoe
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('id invalid - received: undefined')
    }
  })

  test('should error - talla undefined', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_shoe = 'Test marca_shoe'
    const talla_shoe = undefined
    const precio_shoe = 10.75
    const cantidad_shoe = 5
    const colour_shoe = "yellow"


    // Test
    try {
      const response = await updateShoe(
        id,
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe,
        colour_shoe
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('talla_shoe invalid - received: undefined')
    }
  })

  test('should error - precio_shoe undefined', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_shoe = 'Test marca_shoe'
    const talla_shoe = 9
    const precio_shoe = undefined
    const cantidad_shoe = 5
    const colour_shoe = "yellow"

    // Test
    try {
      const response = await updateShoe(
        id,
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe,
        colour_shoe
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('precio_shoe invalid - received: undefined')
    }
  })

  test('should error - cantidad_shoe undefined', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_shoe = 'Test marca_shoe'
    const talla_shoe = 9
    const precio_shoe = 10.75
    const cantidad_shoe = undefined
    const colour_shoe = "yellow"

    // Test
    try {
      const response = await updateShoe(
        id,
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe,
        colour_shoe
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('cantidad_shoe invalid - received: undefined')
    }
  })

  test('should error - colour_shoe undefined', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_shoe = 'Test marca_shoe'
    const talla_shoe = 9
    const precio_shoe = 10.75
    const cantidad_shoe = 8
    const colour_shoe = undefined

    // Test
    try {
      const response = await updateShoe(id,
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe,
        colour_shoe
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('colour_shoe invalid - received: undefined')
    }
  })

  test('should error - Shoes model fails', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_shoe = 'Test marca_shoe'
    const talla_shoe = 9
    const precio_shoe = 10.75
    const cantidad_shoe = 5
    const colour_shoe = "yellow"

    // Mocks
    const mockValue = 'mock value'
    Shoes.findByIdAndUpdate.mockRejectedValue({ message: mockValue })

    // Test
    try {
      const response = await updateShoe(
        id,
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe,
        colour_shoe
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe(mockValue)
    }
  })

  test('should error - Shoes model no fails', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_shoe = 'Test marca_shoe'
    const talla_shoe = 9
    const precio_shoe = 10.75
    const cantidad_shoe = 5
    const colour_shoe = "yellow"

    // Mocks
    Shoes.findByIdAndUpdate.mockResolvedValue()

    // Test
    try {
      const response = await updateShoe(
        id,
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe,
        colour_shoe
      )
      expect(response).toBe(true)
    } catch (error) {
      console.log('TEST error.message:', error.message);
      expect(true).toBe(false)
    }
  })

})