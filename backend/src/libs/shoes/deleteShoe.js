const { Shoes } = require('../../models')

async function deleteShoe(id) {
    try {
        console.log('id:', id)
        console.log('typeof id:', typeof id)
        if (
            typeof id !== 'string'
            || id === ''
        )
            throw {
                message: `id invalid - received: ${JSON.stringify(id)}`
            }

        // Delete Shoe
        // 1.
        // await Shoes.findOneAndDelete({ _id: id })

        // 2.
        await Shoes.findByIdAndDelete(id)
        return true

    } catch (error) {
        throw error
    }
}

module.exports = {
    deleteShoe
}





