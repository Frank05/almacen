const { Shoes } = require('../../models')

async function createShoe(
  marca_shoe,
  talla_shoe,
  precio_shoe,
  cantidad_shoe
) {
  try {
    // Validate marca_shoe
    if (
      typeof marca_shoe !== 'string'
      || marca_shoe === '' 
    ) throw {
      message: `marca_shoe invalid - received: ${JSON.stringify(marca_shoe)}`
    }

    // Validate talla_shoe
    if (
      typeof talla_shoe !== 'number'
      || talla_shoe <= 3
      || talla_shoe >= 15
    ) throw {
      message: `talla_shoe invalid - received: ${JSON.stringify(talla_shoe)}`
    }

    // Validate precio_shoe
    if (
      typeof precio_shoe !== 'number'
      || precio_shoe <= 0
    ) throw {
      message: `precio_shoe invalid - received: ${JSON.stringify(precio_shoe)}`
    }

    // Validate cantidad_shoe
    if (
      typeof cantidad_shoe !== 'number'
      || cantidad_shoe <= 0
    ) throw {
      message: `cantidad_shoe invalid - received: ${JSON.stringify(cantidad_shoe)}`
    }

    // Create hoodie
    await Shoes.create({
      marca_shoe,
      talla_shoe,
      precio_shoe,
      cantidad_shoe,
    })

    // Response
    return true

  } catch (error) {
    throw error
  }
}

module.exports = {
  createShoe
}