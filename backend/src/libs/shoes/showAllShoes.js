const { Shoes } = require('../../models')

async function showAllShoes() {
  try {

    const shoes = await Shoes.find()
    return shoes

  } catch (error) {
    throw error
  }
}

module.exports = {
  showAllShoes
}