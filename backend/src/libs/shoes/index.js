const { createShoe } = require('./createShoe')
const { deleteShoe } = require('./deleteShoe')
const { showAllShoes } = require('./showAllShoes')
const { updateShoe } = require('./updateShoe')


module.exports = {
  createShoe,
  deleteShoe,
  showAllShoes,
  updateShoe
}