const { createShoe } = require('./createShoe')

// Mocks
const { Shoes } = require('../../models/Shoes')
jest.mock('../../models/Shoes')


describe('Testing to createShoe.js', () => {

  test('should error - marca_shoe undefined', async () => {
    // Prepare
    expect.assertions(1)

    const marca_shoe = undefined
    const talla_shoe = 9
    const precio_shoe = 10.75
    const cantidad_shoe = 5

    // Test
    try {
      const response = await createShoe(
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('marca_shoe invalid - received: undefined')
    }
  })

  test('should error - talla_shoe undefined', async () => {
    // Prepare
    expect.assertions(1)

    const marca_shoe = 'Test marca_shoe'
    const talla_shoe = undefined
    const precio_shoe = 10.75
    const cantidad_shoe = 5

    // Test
    try {
      const response = await createShoe(
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('talla_shoe invalid - received: undefined')
    }
  })

  test('should error - precio_shoe undefined', async () => {
    // Prepare
    expect.assertions(1)

    const marca_shoe = 'Test marca_shoe'
    const talla_shoe = 9
    const precio_shoe = undefined
    const cantidad_shoe = 5

    // Test
    try {
      const response = await createShoe(
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('precio_shoe invalid - received: undefined')
    }
  })

  test('should error - cantidad_shoe undefined', async () => {
    // Prepare
    expect.assertions(1)

    const marca_shoe = 'Test marca_shoe'
    const talla_shoe = 9
    const precio_shoe = 10.75
    const cantidad_shoe = undefined

    // Test
    try {
      const response = await createShoe(
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('cantidad_shoe invalid - received: undefined')
    }
  })

  test('should error - Shoes model fails', async () => {
    // Prepare
    expect.assertions(1)

    const marca_shoe = 'Test marca_shoe'
    const talla_shoe = 9
    const precio_shoe = 10.75
    const cantidad_shoe = 5

    // Mocks
    const mockValue = 'mock value'
    Shoes.create.mockRejectedValue({ message: mockValue })

    // Test
    try {
      const response = await createShoe(
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe(mockValue)
    }
  })

  test('should error - Shoes model fails', async () => {
    // Prepare
    expect.assertions(1)

    const marca_shoe = 'Test marca_shoe'
    const talla_shoe = 9
    const precio_shoe = 10.75
    const cantidad_shoe = 5

    // Mocks
    Shoes.create.mockResolvedValue()

    // Test
    try {
      const response = await createShoe(
        marca_shoe,
        talla_shoe,
        precio_shoe,
        cantidad_shoe
      )
      expect(response).toBe(true)
    } catch (error) {
      console.log('TEST error.message:', error.message);
      expect(true).toBe(false)
    }
  })

})