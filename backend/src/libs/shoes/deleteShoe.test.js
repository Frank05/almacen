const { deleteShoe } = require('./deleteShoe')

//Mocks
const { Shoes } = require('../../models')

jest.mock('../../models/Shoes')

describe('Testing to deleteShoe.js', () => {

  test('should error - delete_shoe undefined', async () => {
    // Prepare
    expect.assertions(1)

    const id = undefined


    // Test
    try {
      const response = await deleteShoe(
        id
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('id invalid - received: undefined')
    }
  })

  test('should error - Shoe model fails', async () => {
    // Prepare
    expect.assertions(1)

    const id = 'A19912a'


    // Mocks
    Shoes.findByIdAndDelete.mockResolvedValue()

    // Test
    try {
      const response = await deleteShoe(
        id

      )
      expect(response).toBe(true)
    } catch (error) {
      console.log('TEST error.message:', error.message);
      expect(true).toBe(false)
    }
  })

  test('should error - Hoodies model fails', async () => {
    // Prepare
    expect.assertions(1)

    const id = 'Ax123'


    // Mocks
    const mockValue = 'mock value'
    Shoes.findByIdAndDelete.mockRejectedValue({ message: mockValue })

    // Test
    try {
      const response = await deleteShoe(
        id
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe(mockValue)
    }
  })

})