const { showAllShoes } = require('./showAllShoes')

//Mocks
const { Shoes } = require('../../models')

jest.mock('../../models/Shoes')

describe('Testing to showAllShoes.js', () => {
  test('should error - Shoes model fails', async () => {
    // Prepare
    expect.assertions(1)

    // Mocks
    const mockValue = 'mock value'
    Shoes.find.mockRejectedValue({ message: mockValue })

    // Test
    try {
      const response = await showAllShoes()

      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe(mockValue)
    }
  })

  test('should error - Shoes model no fails', async () => {
    // Prepare
    expect.assertions(1)

    // Mocks
    //const mockValue = 'mock value'
    Shoes.find.mockResolvedValue()

    // Test

    try {
      const response = await showAllShoes(

      )

      console.log('*****TEST response:****', response);
      expect(response).toBe(undefined)
    } catch (error) {
      console.log('TEST error.message:', error.message);
      expect(undefined).toBe(false)
    }
  })

})