const { Shoes } = require('../../models')

async function updateShoe(
  id,
  marca_shoe,
  talla_shoe,
  precio_shoe,
  cantidad_shoe,
  colour_shoe
) {
  try {

    console.log('id:', id);
    console.log('marca_shoe:', marca_shoe);
    console.log('talla_shoe:', talla_shoe);
    console.log('precio_shoe:', precio_shoe);
    console.log('cantidad_shoe:', cantidad_shoe);

    // Validate id
    if (
      typeof id !== 'string'
      || id === ''
    )
      throw {
        message: `id invalid - received: ${JSON.stringify(id)}`
      }

    if (
      typeof marca_shoe !== 'string'
      || marca_shoe === ''
    ) throw {
      message: `marca_shoe invalid - received: ${JSON.stringify(marca_shoe)}`
    }

    // Validate talla_shoe
    if (
      typeof talla_shoe !== 'number'
      || talla_shoe <= 3
      || talla_shoe >= 15
    ) throw {
      message: `talla_shoe invalid - received: ${JSON.stringify(talla_shoe)}`
    }

    // Validate precio_shoe
    if (
      typeof precio_shoe !== 'number'
      || precio_shoe <= 0
    ) throw {
      message: `precio_shoe invalid - received: ${JSON.stringify(precio_shoe)}`
    }

    // Validate cantidad_shoe
    if (
      typeof cantidad_shoe !== 'number'
      || cantidad_shoe <= 0
    ) throw {
      message: `cantidad_shoe invalid - received: ${JSON.stringify(cantidad_shoe)}`
    }

    if (
      typeof colour_shoe !== 'string'
      || colour_shoe === ''
    ) throw {
      message: `colour_shoe invalid - received: ${JSON.stringify(colour_shoe)}`
    }



    // Create hoodie
    await Shoes.findByIdAndUpdate(id, {
      marca_shoe,
      talla_shoe,
      precio_shoe,
      cantidad_shoe,
      colour_shoe
    })

    // Response
    return true
    // res.status(200).json({
    //   message: 'Shoe has been updated successfully',
    //   marca_shoe,
    //   talla_shoe,
    //   precio_shoe,
    //   cantidad_shoe,
    // })

  } catch (error) {
    throw error
  }
}

module.exports = {
  updateShoe
}