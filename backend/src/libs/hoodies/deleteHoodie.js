const { Hoodie } = require('../../models')

async function deleteHoodie(id) {

  try {

    if (
      typeof id !== 'string'
      || id === ''
    )
      throw {
        message: `id invalid - received: ${JSON.stringify(id)}`
      }

    // Delete Shoe
    // 1.
    // await Shoes.findOneAndDelete({ _id: id })

    // 2.
    await Hoodie.findByIdAndDelete(id)
    return true

  } catch (error) {
    throw error
  }

}

module.exports = {
  deleteHoodie
}