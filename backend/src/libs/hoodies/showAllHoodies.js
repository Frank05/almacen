const { Hoodie } = require('../../models')

async function showAllHoodie() {
  try {

    const hoodie = await Hoodie.find()
    console.log('hoodie', hoodie)
    return hoodie

  } catch (error) {
    throw error
  }
}

module.exports = {
  showAllHoodie
}