const { createHoodie } = require('./createHoodie')
const { deleteHoodie } = require('./deleteHoodie')
const { showAllHoodie } = require('./showAllHoodies')

module.exports = {
  createHoodie,
  deleteHoodie,
  showAllHoodie
}