const { Hoodie } = require('../../models')
//const hoodies = require('../../routes/hoodies')

async function createHoodie(
  marca_hoodie,
  talla_hoodie,
  precio_hoodie,
  cantidad_hoodie,
  colour_hoodie
) {
  try {
    // Validate marca_hoodie
    if (
      typeof marca_hoodie !== 'string'
      || marca_hoodie === ''
    ) throw {
      message: `marca_hoodie invalid - received: ${JSON.stringify(marca_hoodie)}`
    }

    // Validate talla_hoodie
    if (
      typeof talla_hoodie !== 'number'
      || talla_hoodie <= 3
      || talla_hoodie >= 15
    ) throw {
      message: `talla_hoodie invalid - received: ${JSON.stringify(talla_hoodie)}`
    }

    // Validate precio_hoodie
    if (
      typeof precio_hoodie !== 'number'
      || precio_hoodie <= 0
    ) throw {
      message: `precio_hoodie invalid - received: ${JSON.stringify(precio_hoodie)}`
    }

    // Validate cantidad_hoodie
    if (
      typeof cantidad_hoodie !== 'number'
      || cantidad_hoodie <= 0
    ) throw {
      message: `cantidad_hoodie invalid - received: ${JSON.stringify(cantidad_hoodie)}`
    }


    if (
      typeof colour_hoodie !== 'string'
      || colour_hoodie === ''
    ) throw {
      message: `colour_hoodie invalid - received: ${JSON.stringify(colour_hoodie)}`
    }

    // Create shoe
    await Hoodie.create({
      marca_hoodie,
      talla_hoodie,
      precio_hoodie,
      cantidad_hoodie,
      colour_hoodie
    })

    // Response
    return true

  } catch (error) {
    throw error
  }
}

module.exports = {
  createHoodie
}