const { updateHoodie } = require('./updateHoodie')

// Mocks
const { Hoodie } = require('../../models/Hoodie')
jest.mock('../../models/Hoodie')


describe('Testing to updateHoodie.js', () => {

  test('should error - marca_hoodie undefined', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_hoodie = undefined
    const talla_hoodie = 9
    const precio_hoodie = 10.75
    const cantidad_hoodie = 5
    const colour_hoodie = "yellow"

    // Test
    try {
      const response = await updateHoodie(
        id,
        marca_hoodie,
        talla_hoodie,
        precio_hoodie,
        cantidad_hoodie,
        colour_hoodie
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('marca_hoodie invalid - received: undefined')
    }
  })

  test('should error - id undefined', async () => {
    // Prepare
    expect.assertions(1)
    const id = undefined
    const marca_hoodie = 'Test marca_shoe'
    const talla_hoodie = 37
    const precio_hoodie = 10.75
    const cantidad_hoodie = 5
    const colour_hoodie = "yellow"


    // Test
    try {
      const response = await updateHoodie(
        id,
        marca_hoodie,
        talla_hoodie,
        precio_hoodie,
        cantidad_hoodie,
        colour_hoodie
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('id invalid - received: undefined')
    }
  })

  test('should error - talla undefined', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_hoodie = 'Test marca_shoe'
    const talla_hoodie = undefined
    const precio_hoodie = 10.75
    const cantidad_hoodie = 5
    const colour_hoodie = "yellow"


    // Test
    try {
      const response = await updateHoodie(
        id,
        marca_hoodie,
        talla_hoodie,
        precio_hoodie,
        cantidad_hoodie,
        colour_hoodie
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('talla_hoodie invalid - received: undefined')
    }
  })

  test('should error - precio_hoodie undefined', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_hoodie = 'Test marca_shoe'
    const talla_hoodie = 9
    const precio_hoodie = undefined
    const cantidad_hoodie = 5
    const colour_hoodie = "yellow"

    // Test
    try {
      const response = await updateHoodie(
        id,
        marca_hoodie,
        talla_hoodie,
        precio_hoodie,
        cantidad_hoodie,
        colour_hoodie
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('precio_hoodie invalid - received: undefined')
    }
  })

  test('should error - cantidad_hoodie undefined', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_hoodie = 'Test marca_shoe'
    const talla_hoodie = 9
    const precio_hoodie = 10.75
    const cantidad_hoodie = undefined
    const colour_hoodie = "yellow"

    // Test
    try {
      const response = await updateHoodie(
        id,
        marca_hoodie,
        talla_hoodie,
        precio_hoodie,
        cantidad_hoodie,
        colour_hoodie
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('cantidad_hoodie invalid - received: undefined')
    }
  })

  test('should error - colour_hoodie undefined', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_hoodie = 'Test marca_shoe'
    const talla_hoodie = 9
    const precio_hoodie = 10.75
    const cantidad_hoodie = 8
    const colour_hoodie = undefined

    // Test
    try {
      const response = await updateHoodie(id,
        marca_hoodie,
        talla_hoodie,
        precio_hoodie,
        cantidad_hoodie,
        colour_hoodie
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('colour_hoodie invalid - received: undefined')
    }
  })

  test('should error - Hoodies model fails', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_hoodie = 'Test marca_shoe'
    const talla_hoodie = 9
    const precio_hoodie = 10.75
    const cantidad_hoodie = 5
    const colour_hoodie = "yellow"

    // Mocks
    const mockValue = 'mock value'
    Hoodie.findByIdAndUpdate.mockRejectedValue({ message: mockValue })

    // Test
    try {
      const response = await updateHoodie(
        id,
        marca_hoodie,
        talla_hoodie,
        precio_hoodie,
        cantidad_hoodie,
        colour_hoodie
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe(mockValue)
    }
  })

  test('should error - Hoodie model no fails', async () => {
    // Prepare
    expect.assertions(1)
    const id = 'A19912a'
    const marca_hoodie = 'Test marca_shoe'
    const talla_hoodie = 9
    const precio_hoodie = 10.75
    const cantidad_hoodie = 5
    const colour_hoodie = "yellow"

    // Mocks
    Hoodie.findByIdAndUpdate.mockResolvedValue()

    // Test
    try {
      const response = await updateHoodie(
        id,
        marca_hoodie,
        talla_hoodie,
        precio_hoodie,
        cantidad_hoodie,
        colour_hoodie
      )
      expect(response).toBe(true)
    } catch (error) {
      console.log('TEST error.message:', error.message);
      expect(true).toBe(false)
    }
  })

})