const { deleteHoodie } = require('./deleteHoodie')

//Mocks
const { Hoodie } = require('../../models')

jest.mock('../../models/Hoodie')

describe('Testing to deleteHoodie.js', () => {

  test('should error - delete_hoodie undefined', async () => {
    // Prepare
    expect.assertions(1)

    const id = undefined


    // Test
    try {
      const response = await deleteHoodie(
        id
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe('id invalid - received: undefined')
    }
  })

  test('should error - Hoodie model fails', async () => {
    // Prepare
    expect.assertions(1)

    const id = 'A19912a'


    // Mocks
    Hoodie.findByIdAndDelete.mockResolvedValue()

    // Test
    try {
      const response = await deleteHoodie(
        id

      )
      expect(response).toBe(true)
    } catch (error) {
      console.log('TEST error.message:', error.message);
      expect(true).toBe(false)
    }
  })

  test('should error - Hoodies model fails', async () => {
    // Prepare
    expect.assertions(1)

    const id = 'Ax123'


    // Mocks
    const mockValue = 'mock value'
    Hoodie.findByIdAndDelete.mockRejectedValue({ message: mockValue })

    // Test
    try {
      const response = await deleteHoodie(
        id
      )
      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe(mockValue)
    }
  })

})