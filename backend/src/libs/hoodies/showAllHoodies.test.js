const { showAllHoodie } = require('./showAllHoodies')

//Mocks
const { Hoodie } = require('../../models')

jest.mock('../../models/Hoodie')

describe('Testing to showAllHoodie.js', () => {
  test('should error - Hoodie model fails', async () => {
    // Prepare
    expect.assertions(1)

    // Mocks
    const mockValue = 'mock value'
    Hoodie.find.mockRejectedValue({ message: mockValue })

    // Test
    try {
      const response = await showAllHoodie()

      console.log('TEST response:', response);
      expect(true).toBe(false)
    } catch (error) {
      expect(error.message).toBe(mockValue)
    }
  })

  test('should error - Hoodie model no fails', async () => {
    // Prepare
    expect.assertions(1)

    // Mocks
    //const mockValue = 'mock value'
    Hoodie.find.mockResolvedValue()

    // Test

    try {
      const response = await showAllHoodie(

      )

      console.log('*****TEST response:****', response);
      expect(response).toBe(undefined)
    } catch (error) {
      console.log('TEST error.message:', error.message);
      expect(undefined).toBe(false)
    }
  })

})