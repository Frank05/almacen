const jwt = require('jsonwebtoken')
const { User } = require('../../models')

async function checkToken(token) {
  try {
    // Validate token
    if (typeof token !== 'string' || token === '') throw {
      message: 'Token invalid'
    }

    // Decrypt token
    const { user, date } = jwt.verify(token, process.env.JWT_SECRET)

    // Validate user
    const userExist = await User.findOne({ email: req.body.email });
        if (userExist) {
            return res.json({ message: 'User already exist with the given emailId' })
        }
    
    // Validate permissions

    // Response
    return user

  } catch (error) {
    throw error
  }
}

module.exports = {
  checkToken
}