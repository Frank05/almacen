const { createToken } = require('./createToken')
const { checkToken } = require('./checkToken')

module.exports = {
  createToken,
  checkToken
}