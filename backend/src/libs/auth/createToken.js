const jwt = require('jsonwebtoken')

function createToken(user) {
  try {
    // Validate user
    if (typeof user !== 'string' || user === '') throw {
      message: `user invalid - receiverd: ${JSON.stringify(user)}`
    }

    // Create token
    const token = jwt.sign(
      {
        user,
        //date: Date.now()
      },
      process.env.JWT_SECRET,
      { expiresIn: '2h' }
    )

    // Response
    return token

  } catch (error) {
    throw {
      ...error,
      functionName: 'createToken',
    }
  }
}

module.exports = {
  createToken
}