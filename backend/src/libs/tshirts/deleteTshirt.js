const { Tshirt } = require("../../models")

async function deleteTshirt(id) {
  try {
    console.log('id:', id)
    console.log('typeof id:', typeof id)

    if (
      typeof id !== 'string'
      || id === ''
    )
      throw {
        message: `id invalid - received: ${JSON.stringify(id)}`
      }

    // Delete Shoe
    // 1.
    await Tshirt.findOneAndDelete({ _id: id })

    // 2.
    //await Tshirt.findByIdAndDelete(id)
    return true

  } catch (error) {
    throw error
  }

}

module.exports = {
  deleteTshirt
}