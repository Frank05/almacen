const { createTshirt } = require('./createTshirt')
const { deleteTshirt } = require('./deleteTshirt')
const { showAllTshirt } = require('./showAllTshirt')
const { updateTshirt } = require('./updateTshirt')

module.exports = {
  createTshirt,
  deleteTshirt,
  showAllTshirt,
  updateTshirt
}