const { Tshirt } = require('../../models')

async function showAllTshirt() {
  try {

    const tshirt = await Tshirt.find()
    return tshirt

  } catch (error) {
    throw error
  }
}

module.exports = {
  showAllTshirt
}