const { Tshirt } = require('../../models')

async function updateTshirt(
  id,
  marca_tshirt,
  talla_tshirt,
  precio_tshirt,
  cantidad_tshirt
) {
  try {
    // Validate id
    if (
      typeof id !== 'string'
      || id === ''
    )
      throw {
        message: `id invalid - received: ${JSON.stringify(id)}`
      }

    if (
      typeof marca_tshirt !== 'string'
      || marca_tshirt === ''
    ) throw {
      message: `marca_tshirt invalid - received: ${JSON.stringify(marca_tshirt)}`
    }

    // Validate talla_tshirt
    if (
      typeof talla_tshirt !== 'number'
      || talla_tshirt <= 3
      || talla_tshirt >= 15
    ) throw {
      message: `talla_tshirt invalid - received: ${JSON.stringify(talla_tshirt)}`
    }

    // Validate precio_tshirt
    if (
      typeof precio_tshirt !== 'number'
      || precio_tshirt <= 0
    ) throw {
      message: `precio_tshirt invalid - received: ${JSON.stringify(precio_tshirt)}`
    }

    // Validate cantidad_tshirt
    if (
      typeof cantidad_tshirt !== 'number'
      || cantidad_tshirt <= 0
    ) throw {
      message: `cantidad_tshirt invalid - received: ${JSON.stringify(cantidad_tshirt)}`
    }
    // Create hoodie
    await Tshirt.findByIdAndUpdate(id, {
      marca_tshirt,
      talla_tshirt,
      precio_tshirt,
      cantidad_tshirt,
    })

    // Response
    return true


  } catch (error) {
    throw error
  }
}

module.exports = {
  updateTshirt
}