const shoes = require('./shoes/')
const hoodie = require('./hoodies/')
const tshirt = require("./tshirts")
const auth = require('./auth')

module.exports = {
  ...shoes,
  ...hoodie,
  ...tshirt,
  ...auth
}