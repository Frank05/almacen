const mongoose = require('mongoose');



(async() => {
  // Waiting for database
  let connected = false
  while (!connected) {
    try {
      const uri = `mongodb://${process.env.MONGO_INITDB_HOST}`
      const options = {
        user: process.env.MONGO_INITDB_ROOT_USERNAME,
        pass: process.env.MONGO_INITDB_ROOT_PASSWORD,
        dbName: process.env.MONGO_INITDB_DATABASE,
      }
      await mongoose.connect(uri, options)
      console.log('Connected with database!');
      connected = true
    } catch (error) {
      console.log('Trying to connect with database...');
      console.log(error);
      await new Promise(resolve => setTimeout(resolve, 3000))
    }
  }
  
  // Synchronice models
 await require('./models')
  
  // Start server
  const { app } = require("./app");
  app.listen(3000, () => {
    console.log('Server ready!');
  });
})() 