const express = require('express')
const cors = require("cors");
const { router } = require('./routes') 
//console.log('router', router)
const app = express()

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Routes ---ojo--
app.use(router)

module.exports = {
  app
}