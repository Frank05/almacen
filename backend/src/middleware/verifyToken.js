const { checkToken } = require('../libs')

function verifyToken(req, res, next) {
  try {
    const token = req.headers["authorization"].split(' ')[1] // "Bearer token"
    const dataVerified = checkToken(token)
    next()
  } catch (error) {
    console.log('Error in middleware:', error.message);
    res.status(500).json({ message: error.message })
  }
}

module.exports = {
  verifyToken
}