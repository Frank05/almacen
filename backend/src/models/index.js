const {Shoes}= require('./Shoes')
const {Tshirt}= require('./Tshirt')
const {Hoodie}= require('./Hoodie')

module.exports = {
    Shoes,
    Tshirt,
    Hoodie
}