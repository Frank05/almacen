const moongose = require('mongoose')

const tshirtSchema = new moongose.Schema({
  marca_tshirt: {
    type: String,
    required: true,
  },
  talla_tshirt: {
    type: Number,
    required: true,
  },
  precio_tshirt: {
    type: Number,
    required: true,
  },
  cantidad_tshirt: {
    type: Number,
    required: false
  },
  colour_tshirt: {
    type: String,
    required: false
  },
}, { timestamps: true })

const Tshirt = moongose.model('Tshirt', tshirtSchema)

module.exports = {
  Tshirt
}