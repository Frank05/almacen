const moongose = require('mongoose')

const shoesSchema = new moongose.Schema({
  marca_shoe: {
    type: String,
    required: true,
  },
  talla_shoe: {
    type: Number,
    required: true,
  },
  precio_shoe: {
    type: Number,
    required: true,
  },
  cantidad_shoe: {
    type: Number,
    required: false
  },
}, { timestamps: true })

const Shoes = moongose.model('Shoes', shoesSchema)

module.exports = {
  Shoes
}