const moongose = require('mongoose')

const hoodieSchema = new moongose.Schema({
  marca_hoodie: {
    type: String,
    required: true,
  },
  talla_hoodie: {
    type: Number,
    required: true,
  },
  precio_hoodie: {
    type: Number,
    required: true,
  },
  cantidad_hoodie: {
    type: Number,
    required: false
  },
  colour_hoodie: {
    type: String,
    required: false
  },
}, { timestamps: true })

const Hoodie = moongose.model('Hoodie', hoodieSchema)

module.exports = {
  Hoodie
}